const jwt = require('jwt-simple');
const User = require('../models/user');
const config = require('../config');

function tokenForUser(user) {
  const timestamp = new Date().getTime();
  return jwt.encode({ sub: user.id, iat: timestamp }, config.secret);
}

exports.signin = function(req, res, next) {
  const { firstName, lastName, email } = req.user;
  res.send({ user: { firstName, lastName, email }, token: tokenForUser(req.user) });
}

exports.signup = function(req, res, next) {
  const { firstName, lastName, email, password } = req.body;

  if(!firstName || !lastName || !email || !password) {
    res.status(422).send({ error: 'You must provide your first name, last name, email and a password.' });
  }

  User.findOne({ email: email }, function(err, existingUser) {
    if(err) {
      return next(err);
    }

    if(existingUser) {
      return res.status(422).send({ error: 'Email is in use' });
    }

    const user = new User({
      firstName: firstName,
      lastName: lastName,
      email: email,
      password: password
    });

    user.save(function(err) {
      if(err) {
        return next(err);
      }

      res.json({ user: { firstName, lastName, email }, token: tokenForUser(user) });
    });
  });
}
