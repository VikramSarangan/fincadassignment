# README #

Please follow the below steps in order to run the assignment:

1) Go to FincadAssignment/client -> Open command prompt/bash -> run the command "npm install" -> run the command "npm run start".

2) Go to FincadAssignment/server -> Open command prompt/bash -> run the command "npm install" -> run the command "npm run dev".

3) Please make sure that MongoDB is running and accepting connections.

4) Open the browser and go to the following URL: http://localhost:8080

Contact vikramtrs@gmail.com if you have any questions.