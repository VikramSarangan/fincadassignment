export const AUTH_USER = 'AUTH_USER';
export const UNAUTH_USER = 'UNAUTH_USER';
export const SIGNIN_ERROR = 'SIGNIN_ERROR';
export const SIGNUP_USER = 'SIGNUP_USER';
export const SIGNUP_ERROR = 'SIGNUP_ERROR';
export const FETCH_TEST_USERS = 'FETCH_TEST_USERS';
export const FETCH_TEST_POSTS = 'FETCH_TEST_POSTS';
export const FETCH_TEST_COMMENTS = 'FETCH_TEST_COMMENTS';
export const FETCH_TEST_ALBUMS = 'FETCH_TEST_ALBUMS';
export const FETCH_TEST_PHOTOS = 'FETCH_TEST_PHOTOS';
export const HANDLE_ALBUM_CLICK = 'HANDLE_ALBUM_CLICK';
export const HANDLE_PHOTO_CLICK = 'HANDLE_PHOTO_CLICK';
export const MODIFY_BREADCRUMB_PATH = 'MODIFY_BREADCRUMB_PATH';
export const CREATE_NEW_POST = 'CREATE_NEW_POST';
export const CREATE_NEW_COMMENT = 'CREATE_NEW_COMMENT';
