import axios from 'axios';
import { browserHistory } from 'react-router';
import { reset } from 'redux-form';

import {
  AUTH_USER,
  SIGNIN_ERROR,
  SIGNUP_ERROR,
  UNAUTH_USER,
  FETCH_TEST_USERS,
  FETCH_TEST_POSTS,
  FETCH_TEST_COMMENTS,
  FETCH_TEST_ALBUMS,
  FETCH_TEST_PHOTOS,
  HANDLE_ALBUM_CLICK,
  HANDLE_PHOTO_CLICK,
  MODIFY_BREADCRUMB_PATH,
  CREATE_NEW_POST,
  CREATE_NEW_COMMENT
} from './types';

const ROOT_URL = 'http://localhost:3090';
const REST_API = 'https://jsonplaceholder.typicode.com';

export function signinUser(email, password) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signin`, { email, password })
      .then(response => {
        dispatch({ type: AUTH_USER });

        const { firstName, lastName, email } = response.data.user;

        localStorage.setItem('token', response.data.token);
        localStorage.setItem('userFirstName', firstName);
        localStorage.setItem('userLastName', lastName);
        localStorage.setItem('userEmail', email);

        browserHistory.push('/dashboard/posts');
      })
      .catch(() => {
        dispatch(signinError('Invalid username/password'));
      });
  }
}

export function signinError(error) {
  return {
    type: SIGNIN_ERROR,
    payload: error
  }
}

export function signupUser({ firstName, lastName, email, password }) {
  return function(dispatch) {
    axios.post(`${ROOT_URL}/signup`, { firstName, lastName, email, password })
      .then(response => {
        dispatch({ type: AUTH_USER });

        const { firstName, lastName, email } = response.data.user;

        localStorage.setItem('token', response.data.token);
        localStorage.setItem('userFirstName', firstName);
        localStorage.setItem('userLastName', lastName);
        localStorage.setItem('userEmail', email);
        
        browserHistory.push('/dashboard/posts');
      })
      .catch(error => dispatch(signupError(error.response.data.error)));
  }
}

export function signupError(error) {
  return {
    type: SIGNUP_ERROR,
    payload: error
  }
}

export function signoutUser() {
  return function(dispatch) {
    localStorage.removeItem('token');
    localStorage.removeItem('userFirstName');
    localStorage.removeItem('userLastName');
    localStorage.removeItem('userEmail');

    dispatch({ type: MODIFY_BREADCRUMB_PATH, payload: [] });
    dispatch({ type: UNAUTH_USER });

    browserHistory.push('/');
  }
}

export function fetchTestUsers() {
  const testUsers = JSON.parse(localStorage.getItem('testUsers'));
  return function(dispatch) {
    if(!testUsers) {
      axios.get(`${REST_API}/users`)
        .then(response => {
          localStorage.setItem('testUsers', JSON.stringify(response.data));
          dispatch({ type: FETCH_TEST_USERS, payload: response.data });
        });
    } else {
      dispatch({ type: FETCH_TEST_USERS, payload: testUsers });
    }
  }
}

export function fetchTestPosts() {
  const testPosts = JSON.parse(localStorage.getItem('testPosts'));
  return function(dispatch) {
    if(!testPosts) {
      axios.get(`${REST_API}/posts`)
        .then(response => {
          localStorage.setItem('testPosts', JSON.stringify(response.data));
          dispatch({ type: FETCH_TEST_POSTS, payload: response.data });
        });
    } else {
      dispatch({ type: FETCH_TEST_POSTS, payload: testPosts });
    }
  }
}

export function fetchTestComments() {
  const testComments = JSON.parse(localStorage.getItem('testComments'));
  return function(dispatch) {
    if(!testComments) {
      axios.get(`${REST_API}/comments`)
        .then(response => {
          localStorage.setItem('testComments', JSON.stringify(response.data));
          dispatch({ type: FETCH_TEST_COMMENTS, payload: response.data });
        });
    } else {
      dispatch({ type: FETCH_TEST_COMMENTS, payload: testComments });
    }
  }
}

export function fetchTestAlbums() {
  const testAlbums = JSON.parse(localStorage.getItem('testAlbums'));
  return function(dispatch) {
    if(!testAlbums) {
      axios.get(`${REST_API}/albums`)
        .then(response => {
          localStorage.setItem('testAlbums', JSON.stringify(response.data));
          dispatch({ type: FETCH_TEST_ALBUMS, payload: response.data });
        });
    } else {
      dispatch({ type: FETCH_TEST_ALBUMS, payload: testAlbums });
    }
  }
}

export function fetchTestPhotos() {
  const testPhotos = JSON.parse(localStorage.getItem('testPhotos'));
  return function(dispatch) {
    if(!testPhotos) {
      axios.get(`${REST_API}/photos`)
        .then(response => {
          localStorage.setItem('testPhotos', JSON.stringify(response.data));
          dispatch({ type: FETCH_TEST_PHOTOS, payload: response.data });
        });
    } else {
      dispatch({ type: FETCH_TEST_PHOTOS, payload: testPhotos });
    }
  }
}

export function handleAlbumClick(props) {
  return function(dispatch) {
    const breadCrumbPath = [
      { path: '/dashboard/albums', label: 'Photo Albums' },
      { path: '', label: props.albumTitle }
    ];

    dispatch({ type: HANDLE_ALBUM_CLICK, payload: props });
    dispatch({ type: MODIFY_BREADCRUMB_PATH, payload: breadCrumbPath });

    browserHistory.push(`/dashboard/photos/${props.albumId}`);
  }
}

export function handlePhotoClick(props) {
  return function(dispatch) {
    const breadCrumbPath = [
      { path: '/dashboard/albums', label: 'Photo Albums' },
      { path: `/dashboard/photos/${props.albumId}`, label: props.albumTitle },
      { path: '', label: props.photoTitle }
    ];

    dispatch({ type: HANDLE_PHOTO_CLICK, payload: props });
    dispatch({ type: MODIFY_BREADCRUMB_PATH, payload: breadCrumbPath });

    browserHistory.push(`/dashboard/photo/${props.photoId}`);
  }
}

export function reconstructBreadCrumbPath(breadCrumbPath) {
  return function(dispatch) {
    dispatch({ type: MODIFY_BREADCRUMB_PATH, payload: breadCrumbPath });
  }
}

export function createNewPost({ postTitle, postBody }, newPostId) {
  return function(dispatch) {
    dispatch({ type: CREATE_NEW_POST, payload: { id: newPostId, title: postTitle, body: postBody } });
    dispatch(reset('newpost'));
  }
}

export function createNewComment(postId, newCommentId, comment) {
  const email = localStorage.getItem('userEmail');
  return function(dispatch) {
    dispatch({ type: CREATE_NEW_COMMENT, payload: { id: newCommentId, postId, body: comment, email } });
    dispatch(reset('newcomment'));
  }
}
