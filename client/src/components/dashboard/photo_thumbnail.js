import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

class PhotoThumbnail extends Component {
  handleClick() {
    const { albumId, albumTitle, photoId, photoTitle, albumPhoto } = this.props;
    this.props.handlePhotoClick({albumId, albumTitle, photoId, photoTitle, albumPhoto});
  }

  render() {
    const { photoId, thumbnailUrl, photoTitle } = this.props;

    return (
      <div className="col-sm-2 photoThumbnailStyle">
        <div className="thumbnail" onClick={this.handleClick.bind(this)}>
          <img className="imgBorder" src={thumbnailUrl} alt={photoTitle} />
          <div className="caption">
            <h6>{photoTitle}</h6>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, actions)(PhotoThumbnail);
