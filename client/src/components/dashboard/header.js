import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';

import * as actions from '../../actions';

class DashboardHeader extends Component {
  componentDidUpdate() {
    {this.props.activeTab === "posts" ? this.props.reconstructBreadCrumbPath([]) : ""}
  }

  render() {
    const userFirstName = localStorage.getItem('userFirstName');
    const userLastName = localStorage.getItem('userLastName');
    const userEmail = localStorage.getItem('userEmail');

    return (
      <ul className="nav nav-tabs dashHeaderMarginTop">
        <li key={1} className={this.props.activeTab === "posts" ? "active" : ""}>
          <Link
            to="/dashboard/posts">
            <strong>Posts</strong>
          </Link>
        </li>
        <li key={2} className={this.props.activeTab === "albums" ? "active" : ""}>
          <Link
            to="/dashboard/albums">
            <strong>Photo Albums</strong>
          </Link>
        </li>
        <li key={3} className="pull-right">
          <Link to="/signout" className="logoutButton btn btn-danger">
            <strong>Logout</strong>
          </Link>
        </li>
        <li key={4} className="disabled pull-right">
          <Link>
            Logged in as <strong>{userFirstName} {userLastName}</strong> ({userEmail})
          </Link>
        </li>
      </ul>
    );
  }
}

export default connect(null, actions)(DashboardHeader);
