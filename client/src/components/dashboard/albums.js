import React, { Component } from 'react';
import { connect } from 'react-redux';

import AlbumThumbnail from './album_thumbnail';
import * as actions from '../../actions';

class Albums extends Component {
  componentDidMount() {
      this.props.reconstructBreadCrumbPath([]);
  }

  renderAlbums() {
    const { testUsers, testAlbums, testPhotos  } = this.props;

    if(!testUsers || !testAlbums || !testPhotos) {
      return <div>Loading...</div>
    }

    return testUsers.map(user => {
      const { name } = user;
      return (
        testAlbums.filter(testAlbum => testAlbum.userId === user.id)
          .map(album => {
            const albumPhotos = testPhotos.filter(photo => photo.albumId === album.id);
            const { thumbnailUrl, title } = albumPhotos[0];
            return (
              <AlbumThumbnail userName={name} thumbnailUrl={thumbnailUrl} albumTitle={title} albumId={album.id} albumPhotos={albumPhotos} />
            );
          })
      );
    });
  }

  render() {
    return (
      <div className="row">
        {this.renderAlbums()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    testUsers: state.testUsers.users,
    testAlbums: state.testAlbums.albums,
    testPhotos: state.testPhotos.photos
  };
}

export default connect(mapStateToProps, actions)(Albums);
