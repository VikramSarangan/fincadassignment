import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Field, reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import Scroll from 'react-scroll';

import * as actions from '../../actions';

const renderField = ({ input, textarea, placeholder, type, meta: { touched, error } }) => (
  <fieldset className="form-group">
    <div>
      {
       type === "textarea"
       ? <textarea className="form-control" placeholder={placeholder} {...input} />
       : <input className="form-control" placeholder={placeholder} {...input} type={type} />
      }
      {touched && error && <span className="error">{error}</span>}
    </div>
  </fieldset>
);

class NewPost extends Component {
  componentDidUpdate(prevProps, prevState) {
    { this.props.testPosts.length > prevProps.testPosts.length ? Scroll.animateScroll.scrollToBottom() : '' }
  }

  handleFormSubmit(formProps) {
    this.props.createNewPost(formProps, this.props.newPostId);
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <div>
        <button className="btn btn-primary newPostButtonMargin" data-toggle="collapse" data-target="#addPost">
          New Post
        </button>
        <div className="collapse addPost" id="addPost">
          <div className="panel panel-default">
            <div className="panel-body">
              <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
                <Field name="postTitle" placeholder="Title" component={renderField} type="text"/>
                <Field name="postBody" placeholder="Body" component={renderField} type="textarea"/>
                <button action="submit" className="btn btn-primary">
                  <strong>Submit</strong>
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

function validate(formProps) {
  const errors = {};

  if(!formProps.postTitle) {
    errors.postTitle = 'Please enter the post title';
  }

  if(!formProps.postBody) {
    errors.postBody = 'Please enter the post body';
  }

  return errors;
}

NewPost = reduxForm({
  form: 'newpost',
  validate
})(NewPost);

function mapStateToProps(state) {
  return {
    testPosts: state.testPosts.posts
  };
}

export default NewPost = connect(mapStateToProps, actions)(NewPost);
