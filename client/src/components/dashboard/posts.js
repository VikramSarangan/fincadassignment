import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';

import Post from './post';
import NewPost from './new_post';

class Posts extends Component {
  renderPosts() {
    const { testUsers, testPosts } = this.props;
    return testPosts.map(post => {
      let userName;
      const postUser = testUsers.filter(user => user.id === post.userId);
      { postUser.length > 0 ? userName=postUser[0].name : userName=`${localStorage.getItem('userFirstName')} ${localStorage.getItem('userLastName')}` }
      return (
        <Post userName={userName} post={post} />
      );
    });
  }

  render() {
    const postIdArray = this.props.testPosts.map(post => post.id);
    const newPostId = Math.max(...postIdArray) + 1;

    return(
      <div>
        <NewPost newPostId={newPostId}/>
        {this.renderPosts()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    testUsers: state.testUsers.users,
    testPosts: state.testPosts.posts
  };
}

export default connect(mapStateToProps)(Posts);
