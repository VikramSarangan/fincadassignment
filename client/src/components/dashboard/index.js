import React, { Component } from 'react';
import DashboardHeader from './header';
import { connect } from 'react-redux';
import Breadcrumb from 'react-breadcrumb';
import { browserHistory } from 'react-router';

import * as actions from '../../actions';

class Dashboard extends Component {
  componentWillMount() {
    this.props.fetchTestUsers();
    this.props.fetchTestPosts();
    this.props.fetchTestComments();
    this.props.fetchTestAlbums();
    this.props.fetchTestPhotos();
  }

  handleBreadCrumbClick(event, {path}) {
    event.preventDefault();
    browserHistory.push(path);
  }

  render() {
    const activeTab = (this.props.location.pathname === "/dashboard/posts" ? "posts" : "albums");
    return (
      <div>
        <DashboardHeader activeTab={activeTab}/>
        {
          this.props.breadCrumbPath && this.props.breadCrumbPath.length > 0
          ? <Breadcrumb
              path={this.props.breadCrumbPath}
              separatorChar={' > '} className="breadcrumb"
              onClick={this.handleBreadCrumbClick.bind(this)} />
          : ""
        }
        {this.props.children}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    breadCrumbPath: state.breadCrumbPath.path
  };
}

export default connect(mapStateToProps, actions)(Dashboard);
