import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

import * as actions from '../../actions';
import PhotoThumbnail from './photo_thumbnail';

class Photos extends Component {
  componentDidMount() {
    const breadCrumbPath = [
      { path: '/dashboard/albums', label: 'Photo Albums' },
      { path: '', label: this.props.albumTitle }
    ];

    this.props.reconstructBreadCrumbPath(breadCrumbPath);

    if(!this.props.albumPhotos) {
      browserHistory.push('/dashboard/albums');
    }
  }

  renderPhotos() {
    let { albumId, albumTitle, albumPhotos } = this.props;

    if(!albumPhotos) {
      return <div>Loading...</div>;
    }

    return(
      albumPhotos.map(photo => {
        const { id, thumbnailUrl, title } = photo;
        return (
          <PhotoThumbnail
            photoId={id}
            thumbnailUrl={thumbnailUrl}
            photoTitle={title}
            albumPhoto={photo}
            albumId={albumId}
            albumTitle={albumTitle} />
        );
      })
    );
  }

  render() {
    return (
      <div>
        <h5 className="albumUserNameMargin"><strong>{this.props.albumUserName}</strong></h5>
        <div className="row">
          {this.renderPhotos()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { albumId, albumTitle, albumUserName, albumPhotos } = state.testAlbums;

  return {
    albumId,
    albumTitle,
    albumUserName,
    albumPhotos
  };
}

export default connect(mapStateToProps, actions)(Photos);
