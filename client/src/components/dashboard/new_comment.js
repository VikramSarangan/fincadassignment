import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { Field, reduxForm, reset } from 'redux-form';
import { connect } from 'react-redux';
import Scroll from 'react-scroll';

import * as actions from '../../actions';

let Element = Scroll.Element;
let scroller = Scroll.scroller;

const renderField = ({ input, textarea, placeholder, type, meta: { touched, error } }) => (
  <fieldset className="form-group">
    <div>
      {
       type === "textarea"
       ? <textarea className="form-control" placeholder={placeholder} {...input} />
       : <input className="form-control" placeholder={placeholder} {...input} type={type} />
      }
      {touched && error && <span className="error">{error}</span>}
    </div>
  </fieldset>
);

class NewComment extends Component {
  shouldComponentUpdate(newProps, newState) {
    const currentNumberOfComments = this.props.testComments.filter(comment => comment.postId === this.props.post.id);
    const newNumberOfComments = newProps.testComments.filter(comment => comment.postId === newProps.post.id);

    if(newNumberOfComments === currentNumberOfComments) {
      return false;
    }

    return true;
  }

  componentDidUpdate(prevProps, prevState) {
    const currentNumberOfComments = this.props.testComments.filter(comment => comment.postId === this.props.post.id);
    const prevNumberOfComments = prevProps.testComments.filter(comment => comment.postId === prevProps.post.id);

    if(currentNumberOfComments > prevNumberOfComments) {
      const commentIdArray = this.props.testComments.map(comment => comment.id);
      const newCommentId = Math.max(...commentIdArray);
      const element = document.getElementById(`c${this.props.post.id}`);
      if(element.innerHTML === "View Comments") {
        element.click();
      }
      scroller.scrollTo(`comment${newCommentId}`, {
        duration: 1500,
        delay: 100,
        smooth: true,
      });
    }
  }

  handleFormSubmit(formProps) {
    const commentIdArray = this.props.testComments.map(comment => comment.id);
    const newCommentId = Math.max(...commentIdArray) + 1;
    const { id } = this.props.post;
    this.props.createNewComment(id, newCommentId, formProps[`newComment${id}`]);
  }

  render() {
    const { handleSubmit, post } = this.props;
    const commentIdArray = this.props.testComments.map(comment => comment.id);
    const newCommentId = Math.max(...commentIdArray);

    return (
      <div key={post.id} className="collapse" id={`addComment${post.id}`}>
        <div className="form-group addComment">
          <form onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <Field name={`newComment${post.id}`} placeholder="Enter Comment" component={renderField} type="textarea"/>
            <button action="submit" className="btn btn-primary addCommentButton">Submit</button>
          </form>
        </div>
      </div>
    );
  }
}

function validate(formProps) {
  const errors = {};

  if(!formProps.newComment) {
    errors.newComment = 'Please enter a comment';
  }

  return errors;
}

NewComment = reduxForm({
  form: 'newcomment',
  validate
})(NewComment);

function mapStateToProps(state) {
  return {
    testPosts: state.testPosts.posts,
    testComments: state.testComments.comments
  };
}

export default NewComment = connect(mapStateToProps, actions)(NewComment);
