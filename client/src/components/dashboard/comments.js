import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import { connect } from 'react-redux';
import Scroll from 'react-scroll';

import * as actions from '../../actions';

let Element = Scroll.Element;

class Comments extends Component {
  shouldComponentUpdate(newProps, newState) {
    const currentNumberOfComments = this.props.testComments.filter(comment => comment.postId === this.props.postId);
    const newNumberOfComments = newProps.testComments.filter(comment => comment.postId === newProps.postId);

    if(currentNumberOfComments === newNumberOfComments) {
      return false;
    }

    return true;
  }

  renderComments() {
    const filteredComments = this.props.testComments.filter(testComment => testComment.postId === this.props.postId);
    return filteredComments.map((comment, index) => {
      return (
        <div key={comment.id} className="panel panel-warning commentPanel">
          <Element name={`comment${comment.id}`}></Element>
          <div className="panel-heading">
            <h6 className="panel-title">Comment {index+1} of {filteredComments.length}</h6>
            <p className="postAuthor">by {comment.userName ? comment.userName : ''} {comment.email}</p>
          </div>
          <div className="panel-body">
            {comment.body}
          </div>
        </div>
      );
    })
  }
  render() {
    return (
      <div>
        {this.renderComments()}
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    testComments: state.testComments.comments
  };
}

export default connect(mapStateToProps, actions)(Comments);
