import React, { Component } from 'react';
import { connect } from 'react-redux';
import { browserHistory } from 'react-router';

class Photo extends Component {
  componentDidMount() {
    if(!this.props.albumPhoto) {
      browserHistory.push('/dashboard/albums');
    }
  }

  renderPhoto() {
    const { albumPhoto } = this.props;

    if(!albumPhoto) {
      return <div>Loading...</div>;
    }

    const { url, title } = albumPhoto;

    return (
      <div className="col-sm-12 photoStyle">
        <img className="imgBorder" src={url} alt={title} />
      </div>
    )
  }

  render() {
    return(
      <div>
        <h5 className="albumUserNameMargin"><strong>{this.props.albumUserName}</strong></h5>
        <div className="row">
          {this.renderPhoto()}
        </div>
      </div>
    );
  }
}

function mapStateToProps(state) {
  const { albumUserName } = state.testAlbums;
  const { photoTitle, albumPhoto } = state.testPhotos;
  return {
    albumUserName,
    photoTitle,
    albumPhoto
  };
}

export default connect(mapStateToProps)(Photo);
