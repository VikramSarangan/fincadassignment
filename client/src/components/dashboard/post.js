import React, { Component } from 'react';
import ReactDOM from 'react-dom';

import Comments from './comments';
import NewComment from './new_comment';

class Post extends Component {
  handleViewCommentButtonClick(postId) {
    if(ReactDOM.findDOMNode(this.refs[`c${postId}`]).innerHTML === "View Comments") {
      ReactDOM.findDOMNode(this.refs[`c${postId}`]).innerHTML = "Close Comments";
    } else {
      ReactDOM.findDOMNode(this.refs[`c${postId}`]).innerHTML = "View Comments";
    }
  }

  render() {
    const { userName, post, handleSubmit } = this.props;

    return (
      <div key={post.id} className="panel panel-info" ref={`post${post.id}`}>
        <div className="panel-heading">
          <h6 className="panel-title"><strong>{post.title}</strong></h6>
          <p className="postAuthor">by {userName}</p>
        </div>
        <div className="panel-body">
          {post.body}
        </div>
        <div className="panel-footer">
          <button className="btn btn-warning postFooterButton viewComments" data-toggle="collapse" ref={`c${post.id}`} id={`c${post.id}`} data-target={`#collapseComments${post.id}`}
             onClick={() => this.handleViewCommentButtonClick(post.id)}>
            View Comments
          </button>
          <button className="btn btn-success postFooterButton" data-toggle="collapse" data-target={`#addComment${post.id}`}>
            Add Comment
          </button>
        </div>
        <div className="collapse" id={`collapseComments${post.id}`}>
          <Comments postId={post.id} />
        </div>
        <NewComment post={post} />
      </div>
    );
  }
}

export default Post;
