import React, { Component } from 'react';
import { connect } from 'react-redux';

import * as actions from '../../actions';

class AlbumThumbnail extends Component {
  handleClick() {
    const { userName, albumId, albumPhotos, albumTitle } = this.props;
    this.props.handleAlbumClick({userName, albumId, albumPhotos, albumTitle});
  }

  render() {
    const { userName, albumTitle, thumbnailUrl, albumId, albumPhotos } = this.props;

    return (
      <div className="col-sm-2 albumThumbnailStyle">
        <div className="thumbnail" onClick={this.handleClick.bind(this)}>
          <img className="imgBorder" src={thumbnailUrl} alt={albumTitle} />
          <div className="caption">
            <h6>{albumTitle}</h6>
            <p>by {userName}</p>
          </div>
        </div>
      </div>
    );
  }
}

export default connect(null, actions)(AlbumThumbnail);
