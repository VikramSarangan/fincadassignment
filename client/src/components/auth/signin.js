import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import { signinUser } from '../../actions';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <fieldset className="form-group">
    <label><strong>{label}</strong></label>
    <div>
      <input className="form-control" {...input} type={type}/>
      {touched && error && <span className="error">{error}</span>}
    </div>
  </fieldset>
);

class Signin extends Component {
  handleFormSubmit({ email, password }) {
    this.props.signinUser(email, password);
  }

  renderAlert() {
      if(this.props.errorMessage) {
        return(
          <div className="alert alert-danger">
            <strong>Oops!</strong> {this.props.errorMessage}
          </div>
        );
      }
  }

  render() {
    const { handleSubmit } = this.props;

    return (
      <form className="signinForm" onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
          <Field name="email" label="Email" component={renderField} type="email"/>

          <Field name="password" label="Password" component={renderField} type="password"/>

          {this.renderAlert()}

          <button action="submit" className="btn btn-primary">
            <strong>Sign In</strong>
          </button>

          <fieldset className="form-group signupLink">
            <label>
              <strong>New user?</strong> Click <Link to="/signup"><strong>here</strong></Link> to signup.
            </label>
          </fieldset>
      </form>
    );
  }
}

function validate(formProps) {
  const errors = {};

  if(!formProps.email) {
    errors.email = 'Please enter an email';
  }

  if(!formProps.password) {
    errors.password = 'Please enter a password';
  }

  return errors;
}

Signin = reduxForm({
  form: 'signin',
  validate
})(Signin);

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.signinError
  };
}

export default Signin = connect(mapStateToProps, { signinUser })(Signin);
