import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { Link } from 'react-router';

import * as actions from '../../actions';

const renderField = ({ input, label, type, meta: { touched, error } }) => (
  <fieldset className="form-group">
    <label><strong>{label}</strong></label>
    <div>
      <input className="form-control" {...input} type={type}/>
      {touched && error && <span className="error">{error}</span>}
    </div>
  </fieldset>
);

class Signup extends Component {
  handleFormSubmit(formProps) {
    this.props.signupUser(formProps);
  }

  renderAlert() {
    if (this.props.errorMessage) {
      return (
        <div className="alert alert-danger">
          <strong>Oops!</strong> {this.props.errorMessage}
        </div>
      )
    }
  }

  render() {
      const { handleSubmit } = this.props;

      return (
        <form className="signupForm" onSubmit={handleSubmit(this.handleFormSubmit.bind(this))}>
            <Field name="firstName" label="First Name" component={renderField} type="text"/>

            <Field name="lastName" label="Last Name" component={renderField} type="text"/>

            <Field name="email" label="Email" component={renderField} type="email"/>

            <Field name="password" label="Password" component={renderField} type="password"/>

            <Field name="passwordConfirm" label="Confirm Password" component={renderField} type="password"/>

            {this.renderAlert()}

            <button action="submit" className="btn btn-primary">
              <strong>Sign Up</strong>
            </button>

            <fieldset className="form-group signinLink">
              <label>
                Click <Link to="/signin"><strong>here</strong></Link> to go back to signin page.
              </label>
            </fieldset>
        </form>
    );
  }
}

function validate(formProps) {
  const errors = {};

  if(!formProps.firstName) {
    errors.firstName = 'Please enter your first name';
  }

  if(!formProps.lastName) {
    errors.lastName = 'Please enter your last name';
  }

  if(!formProps.email) {
    errors.email = 'Please enter an email';
  }

  if(!formProps.password) {
    errors.password = 'Please enter a password';
  }

  if(!formProps.passwordConfirm) {
    errors.passwordConfirm = 'Confirm password field is mandatory';
  }

  if(formProps.password !== formProps.passwordConfirm) {
    errors.passwordConfirm = 'Passwords must match';
  }

  return errors;
}

Signup = reduxForm({
  form: 'signup',
  validate
})(Signup);

function mapStateToProps(state) {
  return {
    errorMessage: state.auth.signupError
  };
}

export default Signup = connect(mapStateToProps, actions)(Signup);
