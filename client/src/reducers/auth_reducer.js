import { AUTH_USER, UNAUTH_USER, SIGNIN_ERROR, SIGNUP_ERROR  } from '../actions/types';

export default function(state={}, action) {
  switch(action.type) {
    case AUTH_USER:
      return { ...state, error: '', authenticated: true };
      break;
    case UNAUTH_USER:
      return { ...state, authenticated: false };
      break;
    case SIGNIN_ERROR:
      return { ...state, signinError: action.payload };
      break;
    case SIGNUP_ERROR:
      return { ...state, signupError: action.payload };
      break;
  }

  return state;
}
