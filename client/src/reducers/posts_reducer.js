import { FETCH_TEST_POSTS, CREATE_NEW_POST } from '../actions/types';
import {reset} from 'redux-form';

export default function(state=[], action) {
  switch(action.type) {
    case FETCH_TEST_POSTS:
      return { ...state, error: '', posts: action.payload };
      break;
    case CREATE_NEW_POST:
      return { ...state, error: '', posts: state.posts.concat(action.payload) };
      break;
  }

  return state;
}
