import { FETCH_TEST_COMMENTS, CREATE_NEW_COMMENT } from '../actions/types';

export default function(state=[], action) {
  switch(action.type) {
    case FETCH_TEST_COMMENTS:
      return { ...state, error: '', comments: action.payload };
      break;
    case CREATE_NEW_COMMENT:
      return { ...state, error: '', comments: state.comments.concat(action.payload) };
      break;
  }

  return state;
}
