import { FETCH_TEST_PHOTOS, HANDLE_PHOTO_CLICK } from '../actions/types';

export default function(state=[], action) {
  switch(action.type) {
    case FETCH_TEST_PHOTOS:
      return { ...state, photos: action.payload };
      break;
    case HANDLE_PHOTO_CLICK:
      return { ...state, photoTitle: action.payload.photoTitle, albumPhoto: action.payload.albumPhoto };
      break;
  }

  return state;
}
