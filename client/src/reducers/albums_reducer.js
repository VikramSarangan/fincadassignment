import { FETCH_TEST_ALBUMS, HANDLE_ALBUM_CLICK } from '../actions/types';

export default function(state=[], action) {
  switch(action.type) {
    case FETCH_TEST_ALBUMS:
      return { ...state, albums: action.payload };
      break;
    case HANDLE_ALBUM_CLICK:
      const { albumId, albumTitle, albumUserName, albumPhotos } = action.payload;
      return { ...state, albumId, albumTitle, albumUserName: action.payload.userName, albumPhotos };
      break;
  }

  return state;
}
