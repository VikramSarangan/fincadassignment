import { FETCH_TEST_USERS } from '../actions/types';

export default function(state=[], action) {
  switch(action.type) {
    case FETCH_TEST_USERS:
      return { ...state, error: '', users: action.payload };
      break;
  }

  return state;
}
