import { combineReducers } from 'redux';
import { reducer as form } from 'redux-form';
import authReducer from './auth_reducer';
import albumsReducer from './albums_reducer';
import usersReducer from './users_reducer';
import photosReducer from './photos_reducer';
import postsReducer from './posts_reducer';
import commentsReducer from './comments_reducer';
import breadCrumbReducer from './breadcrumb_reducer';

const rootReducer = combineReducers({
  form,
  auth: authReducer,
  testUsers: usersReducer,
  testPosts: postsReducer,
  testComments: commentsReducer,
  testAlbums: albumsReducer,
  testPhotos: photosReducer,
  breadCrumbPath: breadCrumbReducer
});

export default rootReducer;
