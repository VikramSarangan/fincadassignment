import { MODIFY_BREADCRUMB_PATH } from '../actions/types';

export default function(state=[], action) {
  switch(action.type) {
    case MODIFY_BREADCRUMB_PATH:
      return { ...state, path: action.payload };
      break;
  }

  return state;
}
