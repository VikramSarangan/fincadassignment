import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/app';
import Signin from './components/auth/signin';
import Signout from './components/auth/signout';
import Signup from './components/auth/signup';
import Dashboard from './components/dashboard';
import Posts from './components/dashboard/posts';
import Albums from './components/dashboard/albums';
import Photos from './components/dashboard/photos';
import Photo from './components/dashboard/photo';
import RequireAuth from './components/auth/require_auth_hoc';

export default (
  <Route path='/' component={App}>
    <IndexRoute component={Signin} />
    <Route path='/signin' component={Signin} />
    <Route path='/signout' component={Signout} />
    <Route path='/signup' component={Signup} />
    <Route path='/dashboard' component={RequireAuth(Dashboard)}>
      <Route path="/dashboard/posts" component={Posts} />
      <Route path="/dashboard/albums" component={Albums} />
      <Route path="/dashboard/photos/:albumId" component={Photos} />
      <Route path="/dashboard/photo/:photoId" component={Photo} />
    </Route>
  </Route>
);
